" Abbreviations for common terms
iabbrev  <buffer> n          numerator
iabbrev  <buffer> d          denominator
iabbrev  <buffer> h          half
iabbrev  <buffer> 1h         1 half
iabbrev  <buffer> /          divided by
iabbrev  <buffer> o          over
iabbrev  <buffer> oo         out of
iabbrev  <buffer> f          fraction
iabbrev  <buffer> l          left
iabbrev  <buffer> r          right
iabbrev  <buffer> w          whereas
iabbrev  <buffer> bc         because
iabbrev  <buffer> e          equals
iabbrev  <buffer> nu         number
iabbrev  <buffer> nus        numbers
iabbrev  <buffer> m          missing
iabbrev  <buffer> s          smaller
iabbrev  <buffer> c          closer
iabbrev  <buffer> hw         however
iabbrev  <buffer> la         larger
iabbrev  <buffer> lat        larger than
iabbrev  <buffer> g          greater
iabbrev  <buffer> gt         greater than
iabbrev  <buffer> b          bigger
iabbrev  <buffer> mt         more than
iabbrev  <buffer> lt         less than
iabbrev  <buffer> uni        (unintelligible)
iabbrev  <buffer> ls         left side
iabbrev  <buffer> rs         right side
iabbrev  <buffer> sil        (silence)
iabbrev  <buffer> ct         close to
iabbrev  <buffer> -          minus
iabbrev  <buffer> t          times
iabbrev  <buffer> mu         multiply
iabbrev  <buffer> ot         on the
iabbrev  <buffer> otr        on the right
iabbrev  <buffer> otl        on the left
iabbrev  <buffer> oot        one on the
iabbrev  <buffer> wb         would be
iabbrev  <buffer> vc         very close
iabbrev  <buffer> si         similar
iabbrev  <buffer> lo         left one
iabbrev  <buffer> ro         right one
iabbrev  <buffer> tt         than the
iabbrev  <buffer> ab         about
iabbrev  <buffer> bt         bigger than
iabbrev  <buffer> fi         figured
iabbrev  <buffer> p          picked
iabbrev  <buffer> acc        accidentally
iabbrev  <buffer> hi         higher
iabbrev  <buffer> ootr       one on the right
iabbrev  <buffer> ootl       one on the left
iabbrev  <buffer> diff       difference
iabbrev  <buffer> app        approximately
iabbrev  <buffer> obv        obviously
iabbrev  <buffer> com        compared
iabbrev  <buffer> th         thought
iabbrev  <buffer> pc         pretty close
iabbrev  <buffer> sim        simplified
iabbrev  <buffer> bw         between
iabbrev  <buffer> al         almost
iabbrev  <buffer> tg         together
iabbrev  <buffer> ar         around
iabbrev  <buffer> di         divide

" Typos
iabbrev  <buffer> numberator numerator

" Prevent accidental actions
inoremap <buffer> <c-space>  <Nop>

" csv formatting
nnoremap <buffer> m          :Copy<enter>
nnoremap <buffer> J          A","<del><esc>
nnoremap <buffer> <c-e>      gg0i"<Esc>G$a"<Esc>gg
