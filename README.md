# vim-fraction-coding

A collection of abbreviations and mappings to make transcribing people
describing fractions easier.

It automatically changes the filetype of `*.cod` files and enables the
following features.

## Abbreviations

| Abbreviation | Expansion          |
| ---          | ---                |
| `n`          | `numerator`        |
| `d`          | `denominator`      |
| `h`          | `half`             |
| `1h`         | `1 half`           |
| `/`          | `divided by`       |
| `o`          | `over`             |
| `oo`         | `out of`           |
| `f`          | `fraction`         |
| `l`          | `left`             |
| `r`          | `right`            |
| `w`          | `whereas`          |
| `bc`         | `because`          |
| `e`          | `equals`           |
| `nu`         | `number`           |
| `nus`        | `numbers`          |
| `m`          | `missing`          |
| `s`          | `smaller`          |
| `c`          | `closer`           |
| `hw`         | `however`          |
| `la`         | `larger`           |
| `lat`        | `larger than`      |
| `g`          | `greater`          |
| `gt`         | `greater than`     |
| `b`          | `bigger`           |
| `mt`         | `more than`        |
| `lt`         | `less than`        |
| `uni`        | `(unintelligible)` |
| `ls`         | `left side`        |
| `rs`         | `right side`       |
| `sil`        | `(silence)`        |
| `ct`         | `close to`         |
| `-`          | `minus`            |
| `t`          | `times`            |
| `mu`         | `multiply`         |
| `ot`         | `on the`           |
| `otr`        | `on the right`     |
| `otl`        | `on the left`      |
| `oot`        | `one on the`       |
| `wb`         | `would be`         |
| `vc`         | `very close`       |
| `si`         | `similar`          |
| `lo`         | `left one`         |
| `ro`         | `right one`        |
| `tt`         | `than the`         |
| `ab`         | `about`            |
| `bt`         | `bigger than`      |
| `fi`         | `figured`          |
| `p`          | `picked`           |
| `acc`        | `accidentally`     |
| `hi`         | `higher`           |
| `ootr`       | `one on the right` |
| `ootl`       | `one on the left`  |
| `diff`       | `difference`       |
| `app`        | `approximately`    |
| `obv`        | `obviously`        |
| `com`        | `compared`         |
| `th`         | `thought`          |
| `pc`         | `pretty close`     |
| `sim`        | `simplified`       |
| `bw`         | `between`          |
| `al`         | `almost`           |
| `tg`         | `together`         |
| `ar`         | `around`           |
| `di`         | `divide`           |

### Typo correction

| Abbreviation | Expansion |
| ---          | ---       |
| `numberator`   | `numerator` |

### Common mistake proofing

```
inoremap <buffer> <c-space>  <Nop>
```

### Commands to make file into csv

> This isn't the best way to do this, but it did the job for my project.

| Binding | Action                  |
| ---     | ---                     |
| `m`     | `:Copy<enter>`          |
| `J`     | `A","<del><esc>`        |
| `<c-e>` | `gg0i"<Esc>G$a"<Esc>gg` |
